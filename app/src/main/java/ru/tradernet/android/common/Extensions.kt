package ru.tradernet.android.common

import android.content.Context
import android.util.Log
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.log10

fun Context.getCompatDrawable(@DrawableRes drawableId: Int) = ContextCompat.getDrawable(this, drawableId)
fun Context.getCompatColor(@ColorRes colorId: Int) = ContextCompat.getColor(this, colorId)
fun RecyclerView.ViewHolder.getContext(): Context = itemView.context
fun Double.getDecimalsAfterComma() = log10(1 / (this % 1 )).toInt()