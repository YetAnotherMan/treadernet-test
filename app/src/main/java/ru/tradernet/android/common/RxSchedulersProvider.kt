package ru.tradernet.android.common

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.*
import io.reactivex.rxjava3.schedulers.Schedulers

class RxSchedulersProvider {

    fun getMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

    fun getIOScheduler(): Scheduler = Schedulers.io()

    fun getComputationScheduler(): Scheduler = Schedulers.computation()

    fun getIoToMainTransformerCompletable(): CompletableTransformer {
        return CompletableTransformer { objectObservable: Completable ->
            objectObservable
                .subscribeOn(getIOScheduler())
                .observeOn(getMainThreadScheduler())
        }
    }

    fun <T> getIoToMainTransformerSingle(): SingleTransformer<T, T> {
        return SingleTransformer { objectObservable: Single<T> ->
            objectObservable
                .subscribeOn(getIOScheduler())
                .observeOn(getMainThreadScheduler())
        }
    }

    fun <T> getIoToMainTransformerFlowable(): FlowableTransformer<T, T> {
        return FlowableTransformer { objectFlowable: Flowable<T> ->
            objectFlowable
                .subscribeOn(getIOScheduler())
                .observeOn(getMainThreadScheduler())
        }
    }

    fun getComputationToMainTransformerCompletable(): CompletableTransformer {
        return CompletableTransformer { objectObservable: Completable ->
            objectObservable
                .subscribeOn(getComputationScheduler())
                .observeOn(getMainThreadScheduler())
        }
    }

    fun <T> getComputationToMainTransformerSingle(): SingleTransformer<T, T> {
        return SingleTransformer { objectObservable: Single<T> ->
            objectObservable
                .subscribeOn(getComputationScheduler())
                .observeOn(getMainThreadScheduler())
        }
    }
}