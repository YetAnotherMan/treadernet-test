package ru.tradernet.android.data.model.model.quotes.remote

import com.google.gson.annotations.SerializedName
import ru.tradernet.android.data.model.model.quotes.Quote

data class RemoteQuote(@SerializedName("c")
                       val c: String,
                       @SerializedName("pcp")
                       val pcp: Float,
                       @SerializedName("ltr")
                       val ltr: String,
                       @SerializedName("name")
                       val name: String,
                       @SerializedName("name2")
                       val name2: String,
                       @SerializedName("ltp")
                       val ltp: Double,
                       @SerializedName("chg")
                       val chg: Double,
                       @SerializedName("min_step")
                       val minStep: Double)

fun RemoteQuote.toQuote() = Quote(c, pcp, ltr, name, name2, ltp, chg, minStep)