package ru.tradernet.android.data.source.socket.base

interface SocketManager {

    fun connect()

    fun disconnect()

    fun isConnected(): Boolean
}
