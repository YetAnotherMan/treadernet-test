package ru.tradernet.android.data.source.stock.quotes.remote

import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import ru.tradernet.android.data.model.model.quotes.Quote
import ru.tradernet.android.data.model.model.quotes.QuoteUpdate
import ru.tradernet.android.data.model.model.quotes.RemoteQuoteUpdate
import ru.tradernet.android.data.model.model.quotes.remote.toQuote
import ru.tradernet.android.data.model.model.quotes.toQuoteUpdates
import ru.tradernet.android.data.source.socket.SocketIOManager
import ru.tradernet.android.data.source.socket.getEvent
import ru.tradernet.android.data.source.stock.quotes.StockQuotesDataSource

class StockQuotesRemoteDataSource(private val service: StockQuotesService,
                                  private val socket: SocketIOManager): StockQuotesDataSource {

    override fun getStockQuotes(tickers: Set<String>): Single<List<Quote>> {
        val t = tickers.joinToString(separator = "+") { it }
        return service.getStockQuotes(t).map { data -> data.map { it.toQuote() } }
    }

    override fun getStockQuoteUpdates(): Flowable<List<QuoteUpdate>> {
        return socket.getEvent<RemoteQuoteUpdate>(EVENT_QUOTE_UPDATES)
            .map { data -> data.toQuoteUpdates() }
    }

    companion object {

        private const val EVENT_QUOTE_UPDATES = "q"
    }
}