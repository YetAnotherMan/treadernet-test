package ru.tradernet.android.data.model.model.quotes

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import ru.tradernet.android.BuildConfig
import java.util.*

@Parcelize
data class Quote(val c: String,
                 val pcp: Float,
                 val ltr: String,
                 val name: String,
                 val name2: String,
                 val ltp: Double,
                 val chg: Double,
                 val minStep: Double): Parcelable

fun Quote.getIconUrl() = "${BuildConfig.IMAGE_BASE_URL}${c.toLowerCase(Locale.getDefault())}"

sealed class QuoteUpdate {

    data class Add(val c: String,
                   val pcp: Float,
                   val ltr: String,
                   val name: String,
                   val name2: String,
                   val ltp: Double,
                   val chg: Double,
                   val minStep: Double): QuoteUpdate()

    data class Update(val c: String, val pcp: Float?, val ltp: Double?, val chg: Double?): QuoteUpdate()
}

fun QuoteUpdate.Add.toQuote() = Quote(c, pcp, ltr, name, name2, ltp, chg, minStep)