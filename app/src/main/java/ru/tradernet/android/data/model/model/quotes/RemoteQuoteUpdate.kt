package ru.tradernet.android.data.model.model.quotes

import com.google.gson.annotations.SerializedName

data class RemoteQuoteUpdate(@SerializedName("q")
                             val updates: List<Update>) {

    data class Update(@SerializedName("c")
                      val c: String,
                      @SerializedName("name")
                      val name: String?,
                      @SerializedName("name2")
                      val name2: String?,
                      @SerializedName("pcp")
                      val pcp: Float?,
                      @SerializedName("ltr")
                      val ltr: String?,
                      @SerializedName("ltp")
                      val ltp: Double?,
                      @SerializedName("chg")
                      val chg: Double?,
                      @SerializedName("min_step")
                      val minStep: Double?)
}

fun RemoteQuoteUpdate.Update.toQuoteUpdates(): QuoteUpdate {
    if (!name.isNullOrEmpty()) {
        return QuoteUpdate.Add(c, pcp ?: 0.0f, ltr ?: "", name, name2 ?: "", ltp ?: 0.0, chg ?: 0.0, minStep ?: 0.0)
    }

    return QuoteUpdate.Update(c, pcp, ltp, chg)
}

fun RemoteQuoteUpdate.toQuoteUpdates(): List<QuoteUpdate> = updates.map { it.toQuoteUpdates() }