package ru.tradernet.android.data.source.stock.quotes

import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import ru.tradernet.android.data.model.model.quotes.Quote
import ru.tradernet.android.data.model.model.quotes.QuoteUpdate

class RealStockQuotesRepository(private val remote: StockQuotesDataSource): StockQuotesRepository {

    override fun getStockQuotes(tickers: Set<String>): Single<List<Quote>> = remote.getStockQuotes(tickers)

    override fun getStockQuoteUpdates(): Flowable<List<QuoteUpdate>> = remote.getStockQuoteUpdates()
}