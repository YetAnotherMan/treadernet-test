package ru.tradernet.android.data.source.socket

import com.google.gson.Gson
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.socket.client.IO
import io.socket.client.Socket
import okhttp3.OkHttpClient
import ru.tradernet.android.data.source.socket.base.SocketManager
import timber.log.Timber

class SocketIOManager(val gson: Gson,
                      private val baseUrl: String,
                      private val okHttpClient: OkHttpClient): SocketManager {

    val socket by lazy {
        val opts = IO.Options().apply {
            reconnection = true
            forceNew = true
            callFactory = okHttpClient
            webSocketFactory = okHttpClient

        }
        val s = IO.socket(baseUrl, opts)
        s.on(Socket.EVENT_CONNECT) {
            Timber.d("Socket connect")
        }.on(Socket.EVENT_DISCONNECT) {
            Timber.d("Socket disconnect")
        }.on(Socket.EVENT_CONNECTING) {
            Timber.d("Socket connecting")
        }.on(Socket.EVENT_CONNECT_TIMEOUT) {
            Timber.d("Socket timeout")
        }.on(Socket.EVENT_PING) {
            Timber.d("Socket ping")
        }.on(Socket.EVENT_PONG) {
            Timber.d("Socket pong")
        }.on(Socket.EVENT_ERROR) {
            Timber.d("Socket error: $it")
        }.on(Socket.EVENT_MESSAGE) {
            Timber.d("Socket message: $it")
        }
        return@lazy s
    }

    override fun connect() {
        if (isConnected()) {
            return
        }

        socket.connect()
    }

    override fun disconnect() {
        socket.disconnect()
    }

    override fun isConnected(): Boolean = socket.connected()
}

fun SocketIOManager.send(event: String, data: Any) {
    socket.emit(event, data)
}

inline fun <reified T : Any> SocketIOManager.getEvent(event: String): Flowable<T> {
    return SocketIOObservable(T::class.java, gson, socket, event)
        .toFlowable(BackpressureStrategy.BUFFER)
}