package ru.tradernet.android.data.source.stock.quotes.remote

import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.tradernet.android.data.model.model.quotes.remote.RemoteQuote

interface StockQuotesService {

    @GET("securities/export")
    fun getStockQuotes(@Query("tickers", encoded = true)
                       tickers: String,
                       @Query("params", encoded = true)
                       params: String = "c+pcp+ltr+name+name2+ltp+chg"): Single<List<RemoteQuote>>
}