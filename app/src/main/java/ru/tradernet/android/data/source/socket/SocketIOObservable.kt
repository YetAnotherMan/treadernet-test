package ru.tradernet.android.data.source.socket

import com.google.gson.Gson
import io.reactivex.rxjava3.android.MainThreadDisposable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.socket.client.Socket
import io.socket.emitter.Emitter

class SocketIOObservable<T>(private val clazz: Class<T>,
                            private val gson: Gson,
                            private val socket: Socket,
                            private val event: String): Observable<T>() {

    override fun subscribeActual(observer: Observer<in T>?) {
        val listener = EmitterListener(socket, event, observer)

        observer?.onSubscribe(listener)

        socket.on(event, listener)
    }

    inner class EmitterListener(private val socket: Socket,
                                private val event: String,
                                private val observer: Observer<in T>?): MainThreadDisposable(), Emitter.Listener {

        override fun onDispose() {
            socket.off(event, this)
        }

        override fun call(vararg args: Any) {
            args.map { gson.fromJson(it.toString(), clazz) as T }.forEach { observer?.onNext(it) }
        }
    }
}