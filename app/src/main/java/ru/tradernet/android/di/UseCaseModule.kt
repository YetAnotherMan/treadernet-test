package ru.tradernet.android.di

import org.koin.dsl.module
import ru.tradernet.android.domain.usecases.GetStockQuotesUpdatesUseCase
import ru.tradernet.android.domain.usecases.GetStockQuotesUseCase

val useCaseModule = module {

    single { GetStockQuotesUseCase(get()) }
    single { GetStockQuotesUpdatesUseCase(get(), get()) }
}