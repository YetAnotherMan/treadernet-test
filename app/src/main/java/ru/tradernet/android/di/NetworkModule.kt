package ru.tradernet.android.di

import coil.util.CoilUtils
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.tradernet.android.BuildConfig
import ru.tradernet.android.TradernetApp
import ru.tradernet.android.data.source.socket.SocketIOManager
import java.util.concurrent.TimeUnit

const val TAG_URL = "TAG_URL"
const val TAG_SOCKET_URL = "TAG_SOCKET_URL"
const val TAG_REGULAR_OKHTTP = "TAG_SOCKET_URL"
const val TAG_COIL_OKHTTP = "TAG_COIL_OKHTTP"
const val TIME_OUT: Long = 30

val networkModule = module {

    single(named(TAG_URL)) { BuildConfig.BASE_URL }
    single(named(TAG_SOCKET_URL)) { BuildConfig.SOCKET_URL }

    single { getGson() }

    single(named(TAG_REGULAR_OKHTTP)) { OkHttpClient.Builder()
        .readTimeout(TIME_OUT, TimeUnit.SECONDS)
        .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
        .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
        .addInterceptor(HttpLoggingInterceptor().apply { level = if (TradernetApp.isDebug()) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE })
        .build() } bind OkHttpClient::class

    single(named(TAG_COIL_OKHTTP)) { OkHttpClient.Builder()
            .cache(CoilUtils.createDefaultCache(get()))
            .addInterceptor(HttpLoggingInterceptor().apply { level = if (TradernetApp.isDebug()) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE })
            .build() } bind OkHttpClient::class

    single { GsonConverterFactory.create(get()) } bind Converter.Factory::class
    single { RxJava3CallAdapterFactory.create() } bind CallAdapter.Factory::class

    single { Retrofit.Builder()
        .baseUrl(get(named(TAG_URL)) as String)
        .client(get(named(TAG_REGULAR_OKHTTP)))
        .addConverterFactory(get())
        .addCallAdapterFactory(get())
        .build()
    }

    single { SocketIOManager(get(), get(named(TAG_SOCKET_URL)), get(named(TAG_REGULAR_OKHTTP))) }
}

fun getGson(): Gson = GsonBuilder()
    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    .create()
