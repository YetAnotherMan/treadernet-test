package ru.tradernet.android.di

import org.koin.dsl.module
import ru.tradernet.android.common.RxSchedulersProvider

val appModule = module {

    single { RxSchedulersProvider() }
}