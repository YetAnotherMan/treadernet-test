package ru.tradernet.android.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.tradernet.android.presentation.stock.quotes.StockQuotesViewModel

val viewModelModule = module {

    viewModel { StockQuotesViewModel(get(), get(), get()) }
}