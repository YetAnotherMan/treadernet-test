package ru.tradernet.android.di

import org.koin.dsl.module
import retrofit2.Retrofit
import ru.tradernet.android.data.source.stock.quotes.remote.StockQuotesService

val networkServiceModule = module {

    single { get<Retrofit>().create(StockQuotesService::class.java) }
}