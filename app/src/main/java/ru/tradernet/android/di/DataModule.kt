package ru.tradernet.android.di

import org.koin.core.qualifier.named
import org.koin.dsl.module
import ru.tradernet.android.data.source.stock.quotes.RealStockQuotesRepository
import ru.tradernet.android.data.source.stock.quotes.StockQuotesDataSource
import ru.tradernet.android.data.source.stock.quotes.StockQuotesRepository
import ru.tradernet.android.data.source.stock.quotes.remote.StockQuotesRemoteDataSource

val dataModule = module {

    single<StockQuotesDataSource>(named<StockQuotesRemoteDataSource>()) { StockQuotesRemoteDataSource(get(), get()) }
    single<StockQuotesRepository> { RealStockQuotesRepository(get(named<StockQuotesRemoteDataSource>())) }
}