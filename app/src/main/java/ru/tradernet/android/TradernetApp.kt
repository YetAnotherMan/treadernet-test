package ru.tradernet.android

import android.app.Application
import android.util.Log
import coil.Coil
import coil.ImageLoader
import coil.ImageLoaderFactory
import coil.util.DebugLogger
import okhttp3.OkHttpClient
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named
import ru.tradernet.android.di.*
import timber.log.Timber

class TradernetApp: Application(), ImageLoaderFactory {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@TradernetApp)
            modules(listOf(appModule, viewModelModule, useCaseModule, dataModule, networkModule, networkServiceModule))
        }

        if (isDebug()) {
            Timber.plant(Timber.DebugTree())
        }

        Coil.setImageLoader(this)
    }

    companion object {

        fun isDebug() = BuildConfig.DEBUG
    }

    override fun newImageLoader(): ImageLoader {
        return ImageLoader.Builder(this)
                .availableMemoryPercentage(0.25)
                .crossfade(true)
                .logger(DebugLogger(if (isDebug()) Log.VERBOSE else Log.ASSERT))
                .okHttpClient(get<OkHttpClient>(named(TAG_REGULAR_OKHTTP)))
                .build()
    }
}