package ru.tradernet.android.domain.usecases

import io.reactivex.rxjava3.core.Flowable
import org.json.JSONArray
import ru.tradernet.android.data.model.model.quotes.Quote
import ru.tradernet.android.data.model.model.quotes.QuoteUpdate
import ru.tradernet.android.data.model.model.quotes.toQuote
import ru.tradernet.android.data.source.socket.SocketIOManager
import ru.tradernet.android.data.source.socket.send
import ru.tradernet.android.data.source.stock.quotes.StockQuotesRepository
import ru.tradernet.android.domain.base.BaseUseCase
import java.util.concurrent.TimeUnit

class GetStockQuotesUpdatesUseCase(private val repository: StockQuotesRepository,
                                   private val socket: SocketIOManager): BaseUseCase<Set<String>, Flowable<List<Quote>>>() {

    private val cache = hashMapOf<String, Quote>()

    override fun invoke(params: Set<String>): Flowable<List<Quote>> {
        return repository.getStockQuoteUpdates()
            .doOnSubscribe {
                cache.clear()
                socket.send("sup_updateSecurities2", JSONArray(params))
            }
            .buffer(2, TimeUnit.SECONDS)
            .map { it.flatten() }
            .map { data ->
                data.forEach { v ->
                    when (v) {
                        is QuoteUpdate.Add -> cache[v.c] = v.toQuote()
                        is QuoteUpdate.Update -> {
                            cache[v.c]?.let {
                                cache[v.c] = it.copy(pcp = v.pcp ?: it.pcp, ltp = v.ltp ?: it.ltp, chg = v.chg ?: it.chg)
                            }
                        }
                    }
                }

                return@map cache.map { it.value }
            }
    }
}