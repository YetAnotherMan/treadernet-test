package ru.tradernet.android.domain.usecases

import io.reactivex.rxjava3.core.Single
import ru.tradernet.android.data.model.model.quotes.Quote
import ru.tradernet.android.data.source.stock.quotes.StockQuotesRepository
import ru.tradernet.android.domain.base.BaseUseCase

class GetStockQuotesUseCase(private val repository: StockQuotesRepository): BaseUseCase<Set<String>, Single<List<Quote>>>() {

    override fun invoke(params: Set<String>): Single<List<Quote>> = repository.getStockQuotes(params)
}