package ru.tradernet.android.domain.base

abstract class BaseUseCase<in Params, out Type> where Type : Any {

    abstract fun invoke(params: Params): Type
}