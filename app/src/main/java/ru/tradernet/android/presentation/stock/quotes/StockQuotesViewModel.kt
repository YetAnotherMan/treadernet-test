package ru.tradernet.android.presentation.stock.quotes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.rxjava3.core.Flowable
import ru.tradernet.android.common.RxSchedulersProvider
import ru.tradernet.android.data.model.model.quotes.Quote
import ru.tradernet.android.data.source.socket.SocketIOManager
import ru.tradernet.android.domain.usecases.GetStockQuotesUpdatesUseCase
import ru.tradernet.android.domain.usecases.GetStockQuotesUseCase
import ru.tradernet.android.presentation.base.BaseViewModel
import timber.log.Timber
import java.util.concurrent.TimeUnit

class StockQuotesViewModel(private val rxSchedulersProvider: RxSchedulersProvider,
                           private val getStockQuotesUpdatesUseCase: GetStockQuotesUpdatesUseCase,
                           private val socketManager: SocketIOManager): BaseViewModel() {

    private val _stockQuotesLiveData = MutableLiveData<List<Quote>>()

    val stockQuotes: LiveData<List<Quote>>
        get() = _stockQuotesLiveData

    init {
        socketManager.connect()

        onLoad()
    }

    private fun onLoad() {
        _disposable.add(getStockQuotesUpdatesUseCase.invoke(TICKERS)
                .compose(rxSchedulersProvider.getIoToMainTransformerFlowable())
                .subscribe(::onDataSuccess, ::onDataError))
    }

    private fun onDataSuccess(data: List<Quote>) {
        _progressLiveData.value = false
        _stockQuotesLiveData.value = data
    }

    private fun onDataError(throwable: Throwable) {
        Timber.e(throwable)
        _progressLiveData.value = false
    }

    override fun onCleared() {
        super.onCleared()
        socketManager.disconnect()
    }

    companion object {

        private val TICKERS = setOf(
            "RSTI","GAZP","MRKZ",
            "RUAL","HYDR","MRKS",
            "SBER","FEES","TGKA",
            "VTBR","ANH.US","VICL.US",
            "BURG.US","NBL.US","YETI.US",
            "WSFS.US","NIO.US","DXC.US",
            "MIC.US","HSBC.US","EXPN.EU",
            "GSK.EU","SHP.EU","MAN.EU",
            "DB1.EU","MUV2.EU","TATE.EU",
            "KGF.EU","MGGT.EU","SGGD.EU")
    }
}