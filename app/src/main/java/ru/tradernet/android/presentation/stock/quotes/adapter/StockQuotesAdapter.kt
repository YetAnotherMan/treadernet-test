package ru.tradernet.android.presentation.stock.quotes.adapter

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import ru.tradernet.android.R
import ru.tradernet.android.common.getCompatColor
import ru.tradernet.android.common.getCompatDrawable
import ru.tradernet.android.common.getContext
import ru.tradernet.android.common.getDecimalsAfterComma
import ru.tradernet.android.data.model.model.quotes.Quote
import ru.tradernet.android.data.model.model.quotes.getIconUrl

class StockQuotesAdapter: ListAdapter<Quote, StockQuotesAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_stock_quote, parent, false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
            return
        }

        val bundle = payloads[0] as? Bundle
        holder.update(bundle?.getParcelable(BUNDLE_QUOTE_OLD), bundle?.getParcelable(BUNDLE_QUOTE_NEW))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val icon: ImageView = itemView.findViewById(R.id.icon)
        private val ticker: TextView = itemView.findViewById(R.id.ticker)
        private val name: TextView = itemView.findViewById(R.id.name)
        private val info1: TextView = itemView.findViewById(R.id.info1)
        private val info2: TextView = itemView.findViewById(R.id.info2)

        @SuppressLint("SetTextI18n")
        fun bind(item: Quote) {
            icon.load(item.getIconUrl()) {
                crossfade(true)
            }

            ticker.text = item.c
            name.text = "${item.ltr} | ${item.name}"

            setPcp(item.pcp)
            setPrice(item)
        }

        @SuppressLint("SetTextI18n")
        fun update(oldItem: Quote?, newItem: Quote?) {
            if (oldItem == null || newItem == null) {
                return
            }

            setPcp(newItem.pcp, oldItem.pcp)
            setPrice(newItem)
        }

        @SuppressLint("SetTextI18n")
        private fun setPcp(newValue: Float, oldValue: Float? = null) {
            info1.text = "${String.format("%+.2f", newValue)}%"
            if (oldValue != null && newValue != oldValue) {
                info1.setTextColor(getContext().getCompatColor(R.color.white))
                info1.background = when {
                    newValue > oldValue -> getContext().getCompatDrawable(R.drawable.bg_quote_positive)
                    newValue < oldValue -> getContext().getCompatDrawable(R.drawable.bg_quote_negative)
                    else -> null
                }

                info1.animate().cancel()
                info1.animate()
                        .setDuration(1000)
                        .withEndAction {
                            if (adapterPosition == -1) {
                                return@withEndAction
                            }

                            setPcp(getItem(adapterPosition).pcp)
                        }
                        .start()
            } else {
                info1.background = null
                info1.setTextColor(getContext().getCompatColor(when {
                    newValue > 0.0f -> R.color.green
                    newValue < 0.0f -> R.color.red
                    else -> R.color.black
                }))
            }
        }

        @SuppressLint("SetTextI18n")
        private fun setPrice(item: Quote) {
            val zerosAfterComma = item.minStep.getDecimalsAfterComma() - 1
            info2.text = "${String.format("%.${zerosAfterComma}f", item.ltp)} ( ${String.format("%+.${zerosAfterComma}f", item.chg)} )"
        }
    }

    override fun getItemId(position: Int): Long = getItem(position).c.hashCode().toLong()

    companion object {

        private const val BUNDLE_QUOTE_OLD = "BUNDLE_QUOTE_OLD"
        private const val BUNDLE_QUOTE_NEW = "BUNDLE_QUOTE_NEW"

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Quote>() {
            override fun areItemsTheSame(oldItem: Quote, newItem: Quote): Boolean = oldItem.c == newItem.c

            override fun areContentsTheSame(oldItem: Quote, newItem: Quote): Boolean = oldItem == newItem

            override fun getChangePayload(oldItem: Quote, newItem: Quote): Any? {
                val bundle = Bundle()
                if (oldItem.pcp != newItem.pcp || oldItem.ltp != newItem.ltp || oldItem.chg != newItem.chg) {
                    bundle.putParcelable(BUNDLE_QUOTE_OLD, oldItem)
                    bundle.putParcelable(BUNDLE_QUOTE_NEW, newItem)
                }

                if (bundle.size() == 0) {
                    return null
                }

                return bundle
            }
        }
    }
}