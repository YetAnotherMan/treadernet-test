package ru.tradernet.android.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class BaseViewModel: ViewModel() {

    protected val _disposable = CompositeDisposable()

    protected val _progressLiveData = MutableLiveData<Boolean>()

    val progress: LiveData<Boolean>
        get() = _progressLiveData

    override fun onCleared() {
        super.onCleared()
        _disposable.dispose()
    }
}