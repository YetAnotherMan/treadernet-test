package ru.tradernet.android.presentation.stock.quotes

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_stock_quotes.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.tradernet.android.R
import ru.tradernet.android.presentation.stock.quotes.adapter.StockQuotesAdapter

class StockQuotesActivity : AppCompatActivity() {

    private val viewModel by viewModel<StockQuotesViewModel>()

    private lateinit var adapter: StockQuotesAdapter

    private val adapterObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            super.onItemRangeInserted(positionStart, itemCount)

            if (itemCount > 1) {
                list.scrollToPosition(positionStart)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stock_quotes)

        setupUI()

        viewModel.progress.observe(this, {
            progress.visibility = if (it) View.VISIBLE else View.GONE
        })

        viewModel.stockQuotes.observe(this, {
            adapter.submitList(it.toMutableList())
        })
    }

    private fun setupUI() {
        adapter = StockQuotesAdapter()
        adapter.setHasStableIds(true)
        adapter.registerAdapterDataObserver(adapterObserver)

        list.adapter = adapter
        list.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }

    override fun onDestroy() {
        super.onDestroy()
        adapter.unregisterAdapterDataObserver(adapterObserver)
    }
}